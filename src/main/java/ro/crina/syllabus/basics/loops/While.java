package ro.crina.syllabus.basics.loops;

public class While {

    /*
    Def: Mod de parcurgere peste o colectie de elemente similare (ex. arrays)

    E nevoie de:
        -o variabila luata ca reper pentru conditia care va fi evaluata
        -o conditie in functie de variabila reper care o sa se evalueze pana cand rezultatul va fi fals
        -o instructiune care schimba valoarea variabilei reper astfel incat conditia sa ajunga la un moment dat sa fie falsa (sa nu avem o bucla infinita)
    Anatomie:
    {initializare variabila reper};
    while ({conditie booleana in functie de variabila reper}) {
        // logica
        {instructiune care schimba valoarea din variabila reper};
    }
     */

    public static void main(String[] args) {

        // numaram pana la 10
        int index = 1; // variabila reper
        while (index <= 10) {
            System.out.println(index);// logica
            index++;// instructiune care schimba valoarea variabilei reper
        }

        // parcurgerea unui array
        String[] names = { "Georgelica", "Adamica", "Marculica", "Ion" };
        int position = 0; // variabila reper; pentru array-uri luam pozitia ca reper
        while (position < names.length) {
            System.out.println(names[position]);// logica
            position++;// schimbarea variabilei reper
        }

        // adunarea elementelor dintr-un sir
        int[] numbers = { 8, 3, 12, 78, 5 };
        int sum = 0;
        int position2 = 0; // reper
        while (position2 < numbers.length) {
            sum += numbers[position2];// logica
            position2++;
        }
        System.out.println("Suma este: " + sum);

        // gasirea maximului dintr-un sir
        int[] numbers3 = { 8, 3, 12, 78, 5 };
        int maximum = numbers3[0];
        int position3 = 0;
        while (position3 < numbers3.length) {
            if (numbers3[position3] > maximum) {
                maximum = numbers3[position3];
            }
            position3++;
        }
        System.out.println("Maximul este: " + maximum);
    }
}
