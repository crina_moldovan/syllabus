package ro.crina.syllabus.basics.loops;

public class For {

    /*
    Def: Mod de parcurgere peste o colectie de elemente similare (ex. arrays)


    E nevoie de:
        -o variabila luata ca reper pentru conditia care va fi evaluata
        -o conditie in functie de variabila reper care o sa se evalueze pana cand rezultatul va fi fals
        -o instructiune care schimba valoarea variabilei reper astfel incat conditia sa ajunga la un moment dat sa fie falsa (sa nu avem o bucla infinita)

    E la fel ca while-ul numai ca cele 3 componente sunt scrise pe aceeasi linie

    Anatomie:
    for ({initializare unei variabile reper}; {conditie in functie de care se executa blockul sau nu}; {instructiune care schimba valoarea variabilei reper}) {
        // logica
    }
    */

    public static void main(String[] args) {

        // numaram de la 18 la 36 din 2 in 2
        for (int index = 18; index <= 36; index += 2) { // cele 3 componente
            System.out.println(index);// logica
        }

        // parcurgerea unui sir
        String[] fruits = { "banane", "mere", "pere", "kiwi", "kaki" };
        for (int position = 0; position < fruits.length; position++) {
            System.out.println(fruits[position]);
        }

        // gasirea minimului dintr-un sir
        int[] numbers = {8, 3, 15, -8, 72, -12, 3};
        int minimum = numbers[0];
        for (int position = 0; position < numbers.length; position++) {
            if (minimum > numbers[position]) {
                minimum = numbers[position];
            }
        }
        System.out.println("Minimul este: " + minimum);
    }
}
