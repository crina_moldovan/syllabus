package ro.crina.syllabus.basics.functions;

public class Functions {

    /*
    Def: O grupare logica de instructiuni care ia un set de parametrii de intrare si poate sa returneze sau nu un rezultat.

    Caracteristici:
        -nume
        -tipul de data returnat, poate sa fie si void
        -setul de parametrii, fiecare parametru functioneaza in interiorul functiei ca o variabila locala

        -** static sau non-static: nu conteaza deocamdata, punem toate functiile statice

    Anatomia unei functii:
    {static daca e necesar} {tipul de data returnat sau void} {numele functiei}({set de parametrii cu tip de data si nume}) {
        {logica}
    }
     */

    public static void main(String[] args) {

        // functie fara return type si fara parametrii
        helloWorld();

        // functie fara return type
        greet("Crina");
        greet("Johann");

        // functie cu return type
        String greeting1 = getGreeting("Crina");
        String greeting2 = getGreeting("Johann");

        // functie cu mai multi parametrii
        int sum1 = add(4, 12); // parametrii sub forma de literal
        int sum2 = add(sum1, 2); // parametru sub forma de variabila
        int sum3 = add(8, 92);
        int multiplication = multiply(3, 8);
        int multiplication2 = multiply(sum2, add(3, 2));// parametru sub forma de functie

        // functie care apeleaza alte functii
        int square1 = squareSimple(5);
        int square2 = squareWithOtherFunction(5);
    }

    static void helloWorld() {
        System.out.println("Hello World");
    }

    static String getGreeting(String name) {
//        String greeting = "Hello, " + name + "!";
//        return greeting;
        return "Hello, " + name + "!";
    }

    static void greet(String name) {
        System.out.println("Hello, " + name + "!");
    }

    static int add(int x, int y) {
//        int sum = x + y;
//        return sum;
        return x + y;
    }

    static int multiply(int a, int b) {
        return a * b;
    }

    static int squareSimple(int x) {
        return x * x;
    }

    static int squareWithOtherFunction(int x) {
        return multiply(x, x);
    }
}
