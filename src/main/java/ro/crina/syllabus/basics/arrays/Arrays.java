package ro.crina.syllabus.basics.arrays;

public class Arrays {

    /*
    Def: Un tip de date care contine un sir de elemente de acelasi tip cu o dimensiune prestabilita
     */

    public static void main(String[] args) {

        // initializare prin alocare de memorie
        int[] numbers = new int[5]; // primeste pe fiecare element valoarea default a tipului de data (int -> 0, boolean -> false, String -> null)
        // initializare inline
        String[] names = { "Crina", "Johann" };

        // accesarea dimensiunii sirului
        int numbersLength = numbers.length; // 5
        int namesLength = names.length; // 2

        // initializarea elementelor din sir prin intregul de pozitie
        numbers[0] = 42;
        numbers[1] = 50;
        numbers[1 + 1] = 60;
        numbers[9 / 3] = 1;
        numbers[numbers.length - 1] = 100; // elementul de pe ultima pozitie

        // accesarea elementelor din sir prin intregul de pozitie
        int number1 = numbers[0];
        int number5 = numbers[numbers.length - 1];
        String name1 = names[0];
        String name2 = names[names.length - 1];
    }
}
