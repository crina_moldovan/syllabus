package ro.crina.syllabus.basics.variables;

public class Objects {

    /*
    Def: Tipuri de date complexe, cu valoare si comportament (pot sa contina si metode)
    Sunt pastrate intr-o alta zona de memorie si in general (exceptie face doar String-ul) sunt initializate cu "new"
    Poate sa ia valoare null (adica lipsa unei valori)
    Ex: String, int[], etc
    Putem sa adaugam si noi obiecte noi
     */

    public static void main(String[] args) {

        String name;
        // valoare:
        name = null;
        name = "Crina";
        name = new String("Crina");
        // comportament/metode:
        name.toUpperCase();
        name.toCharArray();


        Animal dog = new Animal();
        dog.height = 50;
        dog.weight = 20;
        dog.eat();
        dog.eat();
        dog.poop();

        Animal cat = new Animal();
        cat.height = 30;
        cat.weight = 10;
        cat.eat();
    }
}
