package ro.crina.syllabus.basics.variables;

public class Variables {

    /*
    Def: O variabila este un spatiu de memorie in care se pot stoca informatii.
    Caracteristici:
        nume: numele variabilei, expresiv, unic in scope (scope = ultimul nivel de acolade)
        tipul de data: care constrange tipul de informatii pe care le stocheaza variabila
    */


    public static void main(String[] args) {

        // declarare
        int value;
        // dupa declarare variabila inca nu are nici o informatie stocata si nu poate fi accesata. are doar nume si tip.
        // int number = value; // eroare de compilare

        // initializare
        value = 42;
        // dupa initializare variabila poate fi accesata.
        int number = value;
        // 1. initializare cu un literal (literal = valoare hardcodata, ex. 42, false, "asdf", null)
        value = 56;
        // 2. initializare dintr-o alta variabila
        int otherVariable = 45;
        otherVariable += 3;
        value = otherVariable;
        // 3. initializare dintr-o functie
        value = computeValue();

        // declarare si initializare
        int value2 = 43;
        int value3 = value2;
        int value4 = computeValue();
    }

    static int computeValue() {
        // logica
        return 42;
    }
}
