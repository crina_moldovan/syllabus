package ro.crina.syllabus.basics.variables;

public class Numbers {

    /*
    Exista mai multe tipuri de date folosite pentru numere
        Numere intregi:
    -int: cel mai folosit pentru numere intregi (~-2mld -> ~+2mld), ocupa 4 bytes
    -long: folosit pentru numere intregi mai mari (peste ~2mld sau sub ~-2mld), ocupa 8 bytes
    -char: folosit pentru codificare caracterelor ASCII (-128 -> +127), ocupa 1 byte (8 bits)
    -short & byte: folosite foarte rar
        Numere cu virgula:
    -float: cel mai folosit
    -double: folosit cand conteaza precizia
     */

    public static void main(String[] args) {

        // int
        int age = 28;
        int daysPerYear = 365;
        int minutes = 8_325;

        // long
        long earthPopulation = 7_000_000_000L;
        long atoms = 123_000_000_000_000L;

        // char
        char letter = 97;
        char otherLetter = 'a'; // tot 97

        // float
        float footSize = 42.66F;
        float price = 25.34F;

        // double
        double pi = 3.14159265359;
    }
}
