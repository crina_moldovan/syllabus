package ro.crina.syllabus.basics.variables;

public class Strings {

    /*
    Def: String-ul e un tip de data folosit pentru a stoca text

    In spate foloseste un sir de caractere
    String-ul este imutabil. Nu poti modifica valoarea unui string, daca faci operatii pe un string de fapt ti se creeaza stringuri noi.
     */

    public static void main(String[] args) {

        String name = "Crina";

        // daca faci operatii pe stringuri, nu se schimba valoare variabilei pe care faci operatia ci se creeaza un string nou intr-o alta zona de memorie
        name.toUpperCase(); // -> doar creeaza intr-un alt loc stringul "CRINA", name ramane neschimbat;
        System.out.println(name);// -> "Crina"
        // poti sa schimbi valoarea din variabila doar daca ii atribui explicit noul string
        name = name.toUpperCase();
        System.out.println(name);// -> "CRINA";

        String city = "Cluj Napoca";
        // documentatia se poate gasi pe oracle

        // charAt
        char character = city.charAt(3);
        System.out.println(character); // -> 'j'

        // compareTo (returneaza numar negativ daca primul string e inainte de al doilea dpdv alfabetic, pozitiv daca e dupa si 0 daca stringurile sunt egale
        int comparison1 = "Crina". compareTo("Johann"); // -> negativ
        int comparison2 = "Johann".compareTo("Crina"); // -> pozitiv
        int comparison3 = "dinozaur".compareTo("dinozaur"); // -> 0
        System.out.println(comparison1 + " : " + comparison2 + " : " + comparison3);

        // concat (lipire)
        String fullName = "Crina".concat(" ").concat("Moldovan"); // "Crina Moldovan"
        city.concat("asdf"); // "Cluj Napocaasdf"
        city.toUpperCase().concat("asdf"); // "CLUJ NAPOCAasdf"

        // contains
        boolean contains1 = "Crina".contains("in"); // -> true
        boolean contains2 = city.contains("z"); // -> false

        // length
        int length = city.length(); // -> 11

        // toCharArray
        char[] chars = city.toCharArray(); // -> { 'C', 'l', 'u', ..... }

        // toUpperCase, toLowerCase
        city.toUpperCase();
        city.toLowerCase();
    }
}
