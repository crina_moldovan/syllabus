package ro.crina.syllabus.basics.conditions;

public class Conditions {

    /*
    if/else

    Anatomie:
    if ({conditie booleana}) {
        // logica care se executa daca conditia a fost true
    }

    if ({conditie booleana}) {
        // logica care se executa daca conditia a fost true
    } else {
        // logica care se executa daca conditia o fost false
    }

    if ({conditie1}) {
        // logica care se executa daca conditia1 a fost true
    } else if ({conditie2}) {
        // logica care se executa daca conditia1 a fost false si conditia2 a fost true
    } else {
        // logica care se executa daca conditia1 si conditia2 sunt ambele false
    }
     */

    public static void main(String[] args) {

        int value1 = 42;
        if (value1 > 0) {
            System.out.println("The value is greater than 0");
        }

        int value2 = -5;
        if (value2 > 0) {
            System.out.println("The value is greater than 0");
        } else {
            System.out.println("The value is not greater than 0");
        }

        int value3 = 0;
        if (value3 > 0) {
            System.out.println("The value is greater than 0");
        } else if (value3 < 0) {
            System.out.println("The value is less than 0");
        } else {
            System.out.println("The value is 0");
        }

        // with literal
        if (true) {
            // logic
        }
        // with variable
        boolean positive = false;
        if (positive) {
            // logic
        }
        // with function result, function must return boolean
        if (isPositive(53)) {
            // logic
        }
    }

    static boolean isPositive(int x) {
        return x > 0;
    }
}
