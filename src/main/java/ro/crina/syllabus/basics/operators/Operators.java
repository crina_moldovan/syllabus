package ro.crina.syllabus.basics.operators;

public class Operators {
    /*
    Def: Simboluri care desemneaza operatii intre 1, 2 sau 3 valori (in functie de operator)
    Exista o regula de prioritate intre operators: operator precedence table
     */

    public static void main(String[] args) {

        // operatori aritmetici -> opereaza pe numere si returneaza numere
        int sum = 4 + 2; // 6
        int minus = 5 - 6; // -1
        int multiplication = 8 * 3; // 24
        int division = 6 / 2; // 3
        int modulus = 9 % 4; // 1
        int value = 42;
        int incrementPostFix = value++;
        int incrementPreFix = ++value;
        int decrementPostFix = value--;
        int decrementPrefix = --value;

        // operatori relationali -> opereaza pe numere si returneaza un boolean
        boolean equals = 4 == 6;// false
        boolean notEquals = 4 != 6;// true
        boolean greaterThan = 4 > 6;// false
        boolean lessThan = 4 < 6;// true
        boolean greaterThanOrEquals = 5 >= 5;// true
        boolean lessThanOrEquals = 4 <= 6; // true

        // operatori logici -> opereaza pe booleene si returneaza boolean
        boolean and = true && false;// returneaza true cand ambele sunt true
        boolean or = false || true;// returneaza true cand cel putin una din ele este true
        boolean not = !true;// returneaza opusul

        // operators de asignare
        int assign = 42;
        assign += 5;
        assign -= 6;
        assign *= 3;
        assign /= 4;
        assign %= 3;

        // paranteze
        int number = 4 * (5 + 3); // 32

        // member selection pe obiecte -> .
        String name = "Crina".concat(" ").concat("Moldovan");

        // exemple complexe unde ai nevoie de prioritate
        boolean a = 5 > 3 && !false || true == 8 > (7 * 2);
        // a = 5 > 3 && !false || true == 8 > 14
        // a = 5 > 3 && true || true == 8 > 14
        // a = true && true || true == false
        // a = true && true || false
        // a = true || false
        // a = true
    }
}
